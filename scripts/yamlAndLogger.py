#!/usr/bin/env python
# coding: utf-8

# In[7]:


import yaml
import logging


# In[16]:


with open('config.yaml',"r") as f:
    configData = yaml.safe_load(f)


# In[17]:


logging.debug(configData)


# In[18]:


log_path =configData['logging']['path']
logging.debug(log_path)
log_name =configData['logging']['name']
logging.debug(log_name)
log_level =configData['logging']['level']
logging.debug(log_level)


# In[22]:


logging.basicConfig(level = logging.DEBUG,filename=f"{log_name}.log",filemode="w",
                   format = "%(asctime)s - %(levelname)s - %(message)s")


# In[24]:


query_name = configData['query']
logging.debug(query_name)


# In[25]:


for q in query_name:
    logging.info(q)


# In[ ]:




