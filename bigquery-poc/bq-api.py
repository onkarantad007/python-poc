from fastapi import FastAPI, HTTPException, Query
from pydantic import BaseModel
from google.cloud import bigquery
from typing import List,Optional
import datetime
import requests

# Initialize FastAPI app
app = FastAPI()

# Define your BigQuery credentials file
CREDENTIALS_PATH = "focus-sequencer-441823-p4-c537c9218167.json" # Replace with your credentials file path

# Initialize BigQuery client with explicit credentials
client = bigquery.Client.from_service_account_json(CREDENTIALS_PATH)

# Define your BigQuery table
# PROJECT_ID = "focus-sequencer-441823-p4"
# DATASET_ID = "apipoc"
# TABLE_ID = "employee"

# TABLE_REF = f"{PROJECT_ID}.{DATASET_ID}.{TABLE_ID}"
TABLE_REF = "focus-sequencer-441823-p4.apipoc.employee"



# Pydantic model for Employee
class Employee(BaseModel):
    id: int
    name: str
    dept: str
    join_date: datetime.date  # YYYY-MM-DD format


@app.get("/employees", response_model=List[Employee])
async def get_employees(filter_cond: Optional[str] = Query(None), chunk_size: int = 500):
    # Build query
    filter_cond_str = f"{filter_cond}" if filter_cond else ""
    # execute query
    query = f"SELECT id, name, dept, join_date FROM `{TABLE_REF}` {filter_cond_str}"
    try:
        query_job = client.query(query)
        results = query_job.result(page_size=chunk_size)  # Fetch results in chunks
        
        employees = []
        for page in results.pages:
            for row in page:
                employees.append(
                    {
                        "id": row["id"],
                        "name": row["name"],
                        "dept": row["dept"],
                        "join_date": row["join_date"],
                    }
                )
        return employees
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error fetching employees: {str(e)}")
    # try:
    #     query_job = client.query(query)
    #     results = query_job.result()
    #     employees = [dict(row) for row in results]
    #     return employees
    # except Exception as e:
    #     raise HTTPException(status_code=500, detail=f"Error fetching employees: {str(e)}")


@app.post("/employees", response_model=List[Employee])
async def add_employees(employees: List[Employee]):
    """Insert multiple employees into BigQuery."""
    rows_to_insert = [
        {
            "id": employee.id,
            "name": employee.name,
            "dept": employee.dept,
            "join_date": str(employee.join_date),  # Convert date to string for BigQuery
        }
        for employee in employees
    ]
    table = client.get_table(TABLE_REF)  # Fetch table metadata
    try:
        errors = client.insert_rows_json(table, rows_to_insert)  # Insert data
        if errors:
            raise Exception(errors)
        return employees  # Return the list of added employees
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error adding employees: {str(e)}")





def call_post_api(url, payload, headers=None, auth=None):
    """
    Call a POST API with the given URL, payload, headers, and authentication.

    :param url: API endpoint URL
    :param payload: Dictionary containing the payload to send
    :param headers: Dictionary of HTTP headers (optional)
    :param auth: Tuple for basic authentication (username, password) or other auth (optional)
    :return: Response object
    """
    try:
        # Send the POST request
        response = requests.post(url, json=payload, headers=headers, auth=auth)

        # Raise an exception for HTTP errors
        response.raise_for_status()

        # Return the response content (parsed as JSON if possible)
        try:
            return response.json()
        except ValueError:
            return response.text
    except requests.exceptions.RequestException as e:
        print(f"Error calling POST API: {e}")
        return None

# # Example usage
# if __name__ == "__main__":
#     # Define the API endpoint
#     api_url = "https://api.example.com/v1/resource"  # Replace with your API URL

#     # Define the payload
#     payload = {
#         "key1": "value1",
#         "key2": "value2",
#     }

#     # Define headers (optional)
#     headers = {
#         "Authorization": "Bearer your_access_token",  # Replace with your token
#         "Content-Type": "application/json",
#     }

#     # Optional basic authentication (replace with actual credentials if required)
#     auth = ("username", "password")  # Or use None if not needed

#     # Call the API
#     response = call_post_api(api_url, payload, headers=headers, auth=auth)

#     # Print the response
#     if response is not None:
#         print("API Response:", response)
